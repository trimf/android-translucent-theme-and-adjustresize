/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.activity.fragment.recycler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squeakyoak.translucent.common.BaseFragment;
import com.squeakyoak.translucent.translucent.R;
import com.squeakyoak.translucent.utils.BarsHeightHelper;

import butterknife.BindView;

public class RecyclerFragment extends BaseFragment {

    @BindView(R.id.edit_text_container)
    View editTextContainer;

    @BindView(R.id.top_container)
    View topContainer;

    @BindView(R.id.recycle_view)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        topContainer.setPadding(0, BarsHeightHelper.getStatusBarHeight(getActivity()), 0, 0);

        recyclerView.setPadding(
                0,
                BarsHeightHelper.getStatusBarHeight(getActivity()) + getActivity().getResources().getDimensionPixelSize(R.dimen.edit_text_height),
                0,
                BarsHeightHelper.getNavigationAndBottomBarsHeight(getActivity())
        );

        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(
                        getActivity(),
                        LinearLayoutManager.VERTICAL
                )
        );
        recyclerView.setAdapter(new RecyclerAdapter());
    }

    @NonNull
    public static RecyclerFragment newInstance() {
        RecyclerFragment fragment = new RecyclerFragment();
        return fragment;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_recycler;
    }

    @Override
    protected void onKeyboardHeightChanged(int height) {
        super.onKeyboardHeightChanged(height);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) recyclerView.getLayoutParams();
        layoutParams.setMargins(
                layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                height
        );
        recyclerView.setLayoutParams(layoutParams);
    }
}
