/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.squeakyoak.translucent.activity.fragment.recycler.RecyclerFragment;
import com.squeakyoak.translucent.activity.fragment.scrollView.ScrollViewFragment;
import com.squeakyoak.translucent.activity.fragment.simple.SimpleFragment;
import com.squeakyoak.translucent.common.BaseTranslucentActivity;
import com.squeakyoak.translucent.translucent.R;
import com.squeakyoak.translucent.utils.BarsHeightHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseTranslucentActivity {

    @BindView(R.id.bottom_navigation_container)
    View bottomNavigationContainer;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    private final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_simple:
                navigationSimple();
                return true;
            case R.id.navigation_recycler:
                navigationRecycler();
                return true;
            case R.id.navigation_scrollview:
                navigationScollView();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            navigationSimple();
        }

        bottomNavigationContainer.setPadding(
                0,
                0,
                0,
                BarsHeightHelper.getNavigationBarHeight(this)
        );

        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    }

    private void navigationSimple() {
        replaceFragment(SimpleFragment.newInstance());
    }

    private void navigationRecycler() {
        replaceFragment(RecyclerFragment.newInstance());
    }

    private void navigationScollView() {
        replaceFragment(ScrollViewFragment.newInstance());
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }

    @Override
    protected int modifyKeyboardHeight(int height) {
        height -= BarsHeightHelper.getBottomBarHeight(this);
        if (height < 0) {
            height = 0;
        }
        return height;
    }
}
