/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.activity.fragment.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squeakyoak.translucent.translucent.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view)
    TextView textView;

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setItem(int i) {
        textView.setText(itemView.getContext().getString(R.string.recycler_item, i + 1));
    }
}
