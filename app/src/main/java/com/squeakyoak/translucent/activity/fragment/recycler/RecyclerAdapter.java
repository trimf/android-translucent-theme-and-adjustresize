/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.activity.fragment.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squeakyoak.translucent.translucent.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private static final int ITEM_COUNT = 50;

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new RecyclerViewHolder(layoutInflater.inflate(R.layout.item_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int i) {
        recyclerViewHolder.setItem(i);
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }
}
