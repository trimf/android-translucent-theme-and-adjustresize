/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.activity.fragment.simple;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squeakyoak.translucent.common.BaseFragment;
import com.squeakyoak.translucent.translucent.R;
import com.squeakyoak.translucent.utils.BarsHeightHelper;

import butterknife.BindView;

public class SimpleFragment extends BaseFragment {

    @BindView(R.id.edit_text_container)
    View editTextContainer;

    @BindView(R.id.top_container)
    View topContainer;

    @BindView(R.id.text_view)
    View textView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        topContainer.setPadding(0, BarsHeightHelper.getStatusBarHeight(getActivity()), 0, 0);
        textView.setPadding(
                0,
                0,
                0,
                BarsHeightHelper.getNavigationAndBottomBarsHeight(getActivity())
        );
        return view;
    }

    @NonNull
    public static SimpleFragment newInstance() {
        SimpleFragment fragment = new SimpleFragment();
        return fragment;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_simple;
    }

    @Override
    protected void onKeyboardHeightChanged(int height) {
        super.onKeyboardHeightChanged(height);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
        layoutParams.setMargins(
                layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                height
        );
        textView.setLayoutParams(layoutParams);
    }
}
