/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.utils;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.squeakyoak.translucent.translucent.R;

/**
 * Created by a.baskakov play 03/10/2016.
 */

public class BarsHeightHelper {
    private static Integer STATUS_BAR;
    private static Integer NAVIGATION_BAR;
    private static Integer BOTTOM_BAR_HEIGHT;


    public static int getStatusBarHeight(@NonNull Context context) {
        if (STATUS_BAR == null) {
            int statusBarHeight = 0;
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
            }
            STATUS_BAR = statusBarHeight;
        }
        return STATUS_BAR;
    }

    public static int getNavigationBarHeight(@NonNull Context context) {
        if (NAVIGATION_BAR == null) {
            NAVIGATION_BAR = 0;
            try {
                WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                Display display = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    DisplayMetrics realDisplayMetrics = new DisplayMetrics();
                    display.getMetrics(displayMetrics);
                    display.getRealMetrics(realDisplayMetrics);
                    if (displayMetrics.heightPixels != realDisplayMetrics.heightPixels) {
                        NAVIGATION_BAR = realDisplayMetrics.heightPixels - displayMetrics.heightPixels;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return NAVIGATION_BAR;
    }

    public static int getBottomBarHeight(@NonNull Context context) {
        if (BOTTOM_BAR_HEIGHT == null) {
            BOTTOM_BAR_HEIGHT = context.getResources().getDimensionPixelSize(R.dimen.bottom_navigation_height);
        }
        return BOTTOM_BAR_HEIGHT;
    }

    public static int getNavigationAndBottomBarsHeight(Context context) {
        return getBottomBarHeight(context) + getNavigationBarHeight(context);
    }
}
