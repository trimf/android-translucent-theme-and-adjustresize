/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squeakyoak.translucent.keyboard.KeyboardHeightObserver;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    private Unbinder unbinder;

    private final KeyboardHeightObserver keyboardHeightObserver = BaseFragment.this::onKeyboardHeightChanged;

    protected void onKeyboardHeightChanged(int height) {
        //resize content
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        int layout = getLayout();

        if (layout != View.NO_ID) {
            View view = inflater.inflate(getLayout(), container, false);
            unbinder = ButterKnife.bind(this, view);
            return view;
        }
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        Activity activity = getActivity();
        if (activity instanceof BaseTranslucentActivity) {
            ((BaseTranslucentActivity) activity).addKeyboardHeightObserver(keyboardHeightObserver);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Activity activity = getActivity();
        if (activity instanceof BaseTranslucentActivity) {
            ((BaseTranslucentActivity) activity).removeKeyboardHeightObserver(keyboardHeightObserver);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    public abstract int getLayout();
}
