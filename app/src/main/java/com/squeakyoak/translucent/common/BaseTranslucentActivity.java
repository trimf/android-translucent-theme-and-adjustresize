/*
 * ********************************
 * Copyright (c) 2019.
 * trimf
 * ********************************
 *
 */

package com.squeakyoak.translucent.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.squeakyoak.translucent.keyboard.KeyboardHeightObserver;
import com.squeakyoak.translucent.keyboard.KeyboardHeightProvider;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseTranslucentActivity extends AppCompatActivity {

    private Set<KeyboardHeightObserver> keyboardHeightObservers = new HashSet<>();

    /**
     * The keyboard height provider
     */
    @Nullable
    private KeyboardHeightProvider keyboardHeightProvider;

    private final KeyboardHeightObserver keyboardHeightObserver = this::notifyKeyboardHeightChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        keyboardHeightProvider = new KeyboardHeightProvider(this);
        getRootActivityView().post(() -> keyboardHeightProvider.start());
    }

    private @NonNull
    View getRootActivityView() {
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (keyboardHeightProvider != null) {
            keyboardHeightProvider.setKeyboardHeightObserver(keyboardHeightObserver);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (keyboardHeightProvider != null) {
            keyboardHeightProvider.setKeyboardHeightObserver(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (keyboardHeightProvider != null) {
            keyboardHeightProvider.close();
            keyboardHeightProvider = null;
        }
    }

    public void addKeyboardHeightObserver(@NonNull KeyboardHeightObserver observer) {
        keyboardHeightObservers.add(observer);
        if (keyboardHeightProvider != null) {
            int height = modifyKeyboardHeight(keyboardHeightProvider.getCurrentHeight());
            observer.onKeyboardHeightChanged(height);
        }
    }

    public void removeKeyboardHeightObserver(@NonNull KeyboardHeightObserver observer) {
        keyboardHeightObservers.remove(observer);
    }

    private void notifyKeyboardHeightChanged(int height) {
        height = modifyKeyboardHeight(height);
        for (KeyboardHeightObserver keyboardHeightObserver : keyboardHeightObservers) {
            keyboardHeightObserver.onKeyboardHeightChanged(height);
        }
    }

    /**
     * Here you can modify keyboard height value, for example, you have NavigationBar in you activity.
     * You can remove it's height from keyboard height.
     * @param height keyboard height
     * @return modified height
     */
    protected int modifyKeyboardHeight(int height) {
        return height;
    }
}
